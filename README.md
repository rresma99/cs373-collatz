# CS373: Software Engineering Collatz Repo

* Name: Ryan Resma

* EID: rmr3429

* GitLab ID: rresma99

* HackerRank ID: ryan_resma

* Git SHA: 698a45b36ed4868f232ad4bf3fd2c3ead07d3fc4

* GitLab Pipelines: https://gitlab.com/rresma99/cs373-collatz/pipelines

* Estimated completion time: 2.5 hours

* Actual completion time: 3 hours

* Comments: N/A
